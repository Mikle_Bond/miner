﻿#include <QtGui>
#include <QGridLayout>
#include <QToolButton>
#include <QPushButton>
#include <QColor>
#include <iostream>
#include <math.h>
#include <QMouseEvent>


#include "minepaint.h"
#include "button.h"
#include "gamematrix.h"
#include "field.h"
#include "configreader.h"
#include "score.h"
#include "mouseeventfilter.h"


Painter::Painter(const QString & name, size_t buttonsize,  QWidget *parent)
    : QDialog(parent)
    , f(nullptr)
    , buttonfield(nullptr)
    , buttonsize(buttonsize)
    , conf(name)
    , is_game_over(false)
    , fieldSize_x(10)
    , fieldSize_y(10)
{

    Init();
    setWindowTitle("Miner");
}

Painter::~Painter()
{
    delete f;
    delete buttonfield;
}

void Painter::updateButtons()
{
    ButtonHolder &ref = (*buttonfield);
    for(int i = 0; i < fieldSize_x; ++i)
        for(int j = 0; j < fieldSize_y; ++j)
        {
            auto pal = ref[i][j].palette();
            if((*f)[i][j].opened)
            {
                ref[i][j].setEnabled(false);
                switch((*f)[i][j].value) {
                case 0:
                    ref[i][j].setText("");
                    break;
                case Field::MINE:
                    pal.setColor(QPalette::ButtonText, Qt::red);
                    ref[i][j].setPalette(pal);
                    ref[i][j].setText("M");
                    break;
                default:
                    switch((*f)[i][j].value) {
                    case 1:
                        pal.setColor(QPalette::ButtonText, QColor(71, 88, 193));
                        break;
                    case 2:
                        pal.setColor(QPalette::ButtonText, QColor(86, 131, 84));
                        break;
                    case 3:
                        pal.setColor(QPalette::ButtonText, QColor(171, 6, 5));
                        break;
                    case 4:
                        pal.setColor(QPalette::ButtonText, QColor(2, 1, 132));
                        break;
                    case 5:
                        pal.setColor(QPalette::ButtonText, QColor(125, 0, 0));
                        break;
                    case 6:
                        pal.setColor(QPalette::ButtonText, QColor(0, 127, 123));
                        break;
                    default:
                    case 7:
                    case 8:
                        pal.setColor(QPalette::ButtonText, QColor(182, 3, 8));
                        break;
                    }
                    ref[i][j].setPalette(pal);
                    ref[i][j].setText(QString::number((*f)[i][j].value));
                }
            }
            else if((*f)[i][j].flag)
            {
//                pal.setColor(QPalette::ButtonText, QColor(71, 169, 34));
                pal.setColor(QPalette::ButtonText, Qt::magenta);
                ref[i][j].setPalette(pal);
                ref[i][j].setText("F");
                ref[i][j].setEnabled(false);
            }
            else
            {
                ref[i][j].setText("");
                ref[i][j].setEnabled(true);
            }
        }
}

void Painter::fieldButtonClicked()
{
    Button &clickedButton = (*(qobject_cast<Button *>(sender())));
    if(is_game_over)
        return;

    if(!f->open(clickedButton.pos_x, clickedButton.pos_y))
    {
        gameover();
        return;
    }

    updateButtons();

    score->clear();
    score->setText(QString::number(Score::get_instance().get_score()));

    if(f->isLevelSolved())
    {
        NextLevel->setEnabled(true);
    }

    qDebug() << "the button" << clickedButton.pos_x << clickedButton.pos_y;
}

void Painter::rightButtonClicked()
{
    Button &b = (*(qobject_cast<Button *>(sender())));

    if (!(*f)[b.pos_x][b.pos_y].opened) {
        f->toggleFlag(b.pos_x, b.pos_y);
    }

    updateButtons();
}

void Painter::gameover()
{
    for(int i = 0; i < fieldSize_x; ++i)
        for(int j = 0; j < fieldSize_y; ++j)
            (*f)[i][j].opened = true;
    is_game_over = true;
    NextLevel->setEnabled(false);
    updateButtons();
}

void Painter::nextLevel()
{
    resetLevel();
    Score::get_instance().inc_level();
    setupLevel();
}

void Painter::restart()
{
    buttonsize *= fieldSize_x;
    conf = ConfigReader(conf.getName());
    fieldSize_x = conf.top().size_x;
    buttonsize /= fieldSize_x;
    Score::reset_instance();
    Score::get_instance().set_level(0);
    is_game_over = false;
    score->clear();
    nextLevel();
}

void Painter::Init()
{
    QHBoxLayout *optionLayout = new QHBoxLayout;
    basicLayout = new QVBoxLayout;

    NextLevel = new Button;
    NextLevel->setFont(QFont("Mono", 16, QFont::Bold));
    NextLevel->setText("Next Level!");
    NextLevel->setFixedHeight(40);
    connect(NextLevel, SIGNAL(clicked()),this,SLOT(nextLevel()));
    optionLayout->addWidget(NextLevel);

    QPushButton *RestartButton = new QPushButton(QString("RESTART"));
    RestartButton->setFont(QFont("Mono", 16, QFont::Bold));
//    RestartButton->setText("RESART");
    RestartButton->setFixedHeight(40);
    connect(RestartButton, SIGNAL(clicked()),this,SLOT(restart()));
    optionLayout->addWidget(RestartButton);
//    RestartButton->setParent(optionLayout);

    score = new QLineEdit("Score");
    score->setReadOnly(true);
    score->setAlignment(Qt::AlignRight);
    score->setFont(QFont("Times", 16, QFont::Bold));
    score->setMaxLength(20);
    optionLayout->addWidget(score);

    basicLayout->addLayout(optionLayout);
    setLayout(basicLayout);

    setupLevel();
}

void Painter::setupLevel()
{
    if(!conf.empty())
    {
        Params par  = conf.top();
        buttonsize *= fieldSize_x;
        fieldSize_x = par.size_x;
        fieldSize_y = par.size_y;
        buttonsize /= fieldSize_x;
        up_p = par.prob_x;
        sides_p = par.prob_y;
        boombsnum   = par.bombs * fieldSize_x * fieldSize_y;
        conf.pop();
    }
    f = new GameMatrix(fieldSize_x, fieldSize_y, boombsnum, false, up_p, sides_p);
    buttonfield = new ButtonHolder(fieldSize_x, fieldSize_y, buttonsize);
    ButtonHolder &ref = *buttonfield;

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setSizeConstraint(QLayout::SetFixedSize);
    mainLayout->setSpacing(0);

    QHBoxLayout *wideLayout = new QHBoxLayout;
    wideLayout->addStretch();
    wideLayout->addLayout(mainLayout);
    wideLayout->addStretch();

    QPalette pal = ref[0][0].palette();
    pal.setColor(QPalette::Button, QColor(Qt::blue));
    //pal.setColor(QPalette::ButtonText, QPalette::ButtonTextText, QColor(Qt::red));

    for(int i = 0; i < fieldSize_x; ++i)
        for(int j = 0; j < fieldSize_y; ++j)
        {
            ref[i][j].installEventFilter(new MouseFilter(&ref[i][j]));
            ref[i][j].setAutoFillBackground(true);
            ref[i][j].setPalette(pal);
            ref[i][j].setFont(QFont("Mono", buttonsize/2, QFont::Bold));
            ref[i][j].update();
            connect(&ref[i][j], SIGNAL(clicked()),this,SLOT(fieldButtonClicked()));
            connect(&ref[i][j], SIGNAL(flagToggled()),this,SLOT(rightButtonClicked()));
            connect(&ref[i][j], SIGNAL(dblClicked()),this,SLOT(doubleButtonClicked()));
            mainLayout->addWidget(&((*buttonfield)[i][j]),i,j);
        }

    buttonsHolder = new QWidget;

    buttonsHolder->setLayout(wideLayout);
    basicLayout->addWidget(buttonsHolder);
    NextLevel->setEnabled(false);
}

void Painter::resetLevel()
{
    delete f;
    delete buttonfield;
    delete buttonsHolder;
}

void Painter::doubleButtonClicked()
{
    Button &b = (*(qobject_cast<Button *>(sender())));
    qDebug() << "Button double-clicked" << b.pos_x << b.pos_y;
    if (is_game_over)
        return;
    if (!f->openNear(b.pos_x, b.pos_y)) {
        gameover();
        return;
    }
    if(f->isLevelSolved())
        NextLevel->setEnabled(true);
    updateButtons();
}
