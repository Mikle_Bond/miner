#pragma once 
#include "dd.hpp"
#include "field.h"
#include "pathmaker.h"
#include <functional>

/* matrix.h
 * This is simple example on how to use class DD.
 *
 * @author Mikle_Bond
 */

using func_t = std::function<void(size_t,size_t)>;

class GameMatrix;

template <>
struct DD_traits<GameMatrix> 
{
	using ret_type = Field;
};

class GameMatrix : public DD<GameMatrix>
{
public: 
        explicit GameMatrix( size_t x, size_t y, int b_num, 
			bool double_path = false, unsigned up_p = 40, unsigned sides_p = 30 );
	// Opens the field, and all neighbours, if this field is 0-valued.
	bool open (size_t i, size_t j);
	// Toggles the flag of field. Retutns the new state of flag.
	bool toggleFlag(size_t x, size_t y);
	// Tests the matrix, if the path from the top to bottom exists.
	bool isLevelSolved();
	// Opens neighbours of oppened field, if all mines near are flaged.
	bool openNear(size_t x, size_t y);
private:
	int bombs_num;
	bool lvl_solved;
	void setMine(size_t x, size_t y);
	void forEachAround(size_t x, size_t y, func_t f);
	inline void safeApply(size_t x, size_t y, func_t f);
};

