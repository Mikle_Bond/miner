﻿#ifndef MINEPAINT
#define MINEPAINT
#include <QDialog>
#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>

#include "button.h"
#include "gamematrix.h"
#include "buttonholder.h"
#include "configreader.h"
class Painter : public QDialog
{
    Q_OBJECT
public:
    Painter(const QString & name, size_t buttonsize = 55, QWidget *parent = 0);
    ~Painter();
    void updateButtons();


private slots:
    void fieldButtonClicked();
    void rightButtonClicked();
    void doubleButtonClicked();
    void nextLevel();
    void restart();
private:
    ConfigReader conf;
    QLineEdit *score;
    QVBoxLayout *basicLayout;
    int fieldSize_x;
    int fieldSize_y;
    int boombsnum;
    int buttonsize;
    unsigned up_p;
    unsigned sides_p;
    bool is_game_over;
    void gameover();
    //Button createButton();
    GameMatrix *f;
    ButtonHolder *buttonfield;
    Button *NextLevel;
    void Init();
    void setupLevel();
    void resetLevel();
    QWidget *buttonsHolder;

};


#endif // MINEPAINT

