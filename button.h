﻿#ifndef BUTTON
#define BUTTON

#include <QPushButton>

class Button : public QPushButton
{
    Q_OBJECT

signals:
    void flagToggled();
    void dblClicked();

public:
    Button(int x = 0, int y = 0, int size = 100, const QString &text = "&", QWidget *parent = 0);

    void toggleFlag();
    void dblClick();

    QSize sizeHint() const;
    int pos_x;
    int pos_y;
    int button_size;
};

#endif // BUTTON

