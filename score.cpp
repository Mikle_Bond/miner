#include"score.h"

class ScoreResetter
{
    public:
        ~ScoreResetter()
        {
            Score::reset_instance();
        }
};

static ScoreResetter private_instance;

Score *Score::instance = nullptr;

Score &Score::get_instance()
{
    if( instance == nullptr )
        instance = new Score();
    return *instance;
}

void Score::reset_instance()
{
    delete instance;
    instance = nullptr;
}

void Score::add_score()
{
    score_ += level_;
}
