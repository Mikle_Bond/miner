#pragma once
/*
 * score.h
 * Score is a singletone class which calculates the game score
 *
 * author@ qwexfour
 */

class Score
{
    public:
        static Score &get_instance();
        static void reset_instance();
        void add_score();
        int get_score()
        {
            return score_;
        }
        void set_level( int level )
        {
            level_ = level;
        }
        int get_level()
        {
            return level_;
        }
        void inc_level()
        {
            level_++;
        }
    private:
        int score_, level_;
        Score() : score_( 0 ), level_( 1 ) {}
        static Score *instance;
};
