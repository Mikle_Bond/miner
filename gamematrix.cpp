#include "gamematrix.h"
#include "score.h"

#include <random>

#define isSafe(x, y) (x < size_x && y < size_y)

GameMatrix::GameMatrix(size_t x, size_t y, int b_num, bool double_path, unsigned up_p, unsigned sides_p)
	: DD<GameMatrix>(x, y) 
	, bombs_num(b_num)
	, lvl_solved(false)
{
	PathMaker path(x, y, double_path, up_p, sides_p);

	int bombs_curr = 0;
	while (bombs_curr < bombs_num) {
		int i = rand() % size_x;
		int j = rand() % size_y;
		if (path[i][j] || body_[i][j].value == Field::MINE) 
			continue;
		setMine(i, j);
		bombs_curr++;
	}
}

void GameMatrix::setMine(size_t x, size_t y) 
{
	safeApply(x, y, [this](size_t i, size_t j) {body_[i][j].value = Field::MINE;});
	forEachAround(x, y, [this](size_t i, size_t j){
		if (body_[i][j].value != Field::MINE)
			body_[i][j].value += 1;
	});
}

void GameMatrix::forEachAround(size_t x, size_t y, func_t f) 
{
	safeApply(x+1, y+1, f);
	safeApply(x+1, y+0, f);
	safeApply(x+1, y-1, f);
	safeApply(x+0, y+1, f);
	safeApply(x+0, y-1, f);
	safeApply(x-1, y+1, f);
	safeApply(x-1, y+0, f);
	safeApply(x-1, y-1, f);
}

void GameMatrix::safeApply(size_t x, size_t y, func_t f) 
{
	if (isSafe(x, y)) f(x, y);
}

bool GameMatrix::open(size_t i, size_t j)
{
	if (!isSafe(i, j))
		return false;
	if (body_[i][j].flag || body_[i][j].opened)
		return true;
	body_[i][j].opened = true;
	if (body_[i][j].value == Field::MINE)
		return false;
	Score::get_instance().add_score();
	// Here we just open the neighbours with the algorithm above
	if (body_[i][j].value == 0)
		forEachAround(i, j, [this](size_t x, size_t y){ open(x, y); });
	return true;
}

bool GameMatrix::openNear(size_t x, size_t y)
{
	// If someone don't know, what is he doing, he sould get false
	if (!isSafe(x, y))
		return false;
	// If someone tries to open wrong cell, so, he is just wrong.
	if (body_[x][y].flag || !body_[x][y].opened)
		return true;
	// Let's count the flags
	int flags_num = 0;
	forEachAround(x, y, [this, &flags_num](size_t i, size_t j) {
		if (body_[i][j].flag) ++flags_num;
	});
	// Once again, the unprepared cell is just mistake of user, not developer
	if (flags_num != body_[x][y].value)
		return true;
	bool res = true;
	forEachAround(x, y, [this, &res](size_t i, size_t j) {
		res = res && open(i, j);
	});
	return res;
}

bool GameMatrix::toggleFlag(size_t x, size_t y)
{
	if (isSafe(x, y)) 
		return body_[x][y].flag = !body_[x][y].flag;
	else 
		return false;
}

bool GameMatrix::isLevelSolved()
{
	if (lvl_solved)
		return true;
	// Here we should travel trough the matrix, from the top to the bottom, using left-hand algorithm
	// of lurking throurh labirinth. To do so, we are looking for the enterance at the top edge, and then 
	// searching for the exit on the bottom. 
	size_t top_enterance = 0;
	while (true) {
		while (top_enterance < size_y && !body_[0][top_enterance].opened)
			++top_enterance;
		if (top_enterance >= size_y)
			break;
		size_t curr_x = 0, curr_y = top_enterance;
		int dir_x = 1, dir_y = 0, temp = 0;
		size_t new_x, new_y;
		// Next part of algorithm:
		// * turn left
		// * move if you can
		// * if you can not, do
		//   * turn right, try to move (it is forvard)
		//   * turn right and try again (it is on the right)
		//   * you got to trap; turn right and go back;
		// Algorithm stops, if you reach top or bottom edge.
		do {
#define turn_right(x, y) temp = y, y = x, x = -temp
#define turn_left(x, y) turn_right(y, x)
#define look new_x = dir_x + curr_x, new_y = dir_y + curr_y
#define move curr_x = new_x, curr_y = new_y
#define can_go isSafe(new_x, new_y) && body_[new_x][new_y].opened
			turn_left(dir_x, dir_y);
			look;
			if (can_go) {
				move;
			} else {
				turn_right(dir_x, dir_y);
				look;
				if (can_go) {
					move;
				} else {
					turn_right(dir_x, dir_y);
					look;
					if (can_go) {
						move;
					} else {
						turn_right(dir_x, dir_y);
						look;
						move;
					}
				}
			}
#undef can_go
#undef turn_right
#undef turn_left
#undef look
#undef move
		} while (curr_x > 0 && curr_x < size_x - 1);
		if (curr_x == size_x - 1) {
			lvl_solved = true;
			break;
		}
		++top_enterance;
	}
	return lvl_solved;
}

