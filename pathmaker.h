#pragma once
/*pathmaker.h
 *Lib includes class PathMaker. Wich is a randomly made boolean double-dimensional matrix.
 *Which contains true if the cell is a part of the way and false otherwise.
 *User can custom size, is the path double or single, and moving probabilities.
 *
 *author@ qwexfour
 */


#include"dd.hpp"
#include<cassert>

class PathMaker;

template<>
struct DD_traits<PathMaker> 
{
	using ret_type = bool;
};

class PathMaker : public DD<PathMaker>
{
    public:
        explicit PathMaker( size_t x, size_t y, bool double_path = false, unsigned int up_p = 40, unsigned int sides_p = 30 ) : DD<PathMaker>( x, y ), up_p_( up_p ), sides_p_( sides_p ), double_path_( double_path )
        {
            fillPath();
        }
        unsigned int get_length()
        {
            return length_;
        }
    private:
        enum Dir { UP, RIGHT, DOWN, LEFT };
        const unsigned int up_p_;  //moving up probability
        const unsigned int sides_p_;  //moving right probability = moving left probability
        const bool double_path_;      //true if you need to double path
        unsigned int length_;         //the number of cells set with true ( the amount of truth:) )
        void fillPath();
        int chooseNext( bool *youCanMove );
        inline int reverseDir( int dir )
        {
            assert( dir >= 0 && dir < 4 );
            return ( dir + 2 ) % 4;
        }
};

