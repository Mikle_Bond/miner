﻿#include <QtGui>

#include "button.h"

Button::Button(int x, int y, int size, const QString &text, QWidget *parent)
    : QPushButton(parent), pos_x(x), pos_y(y), button_size(size)
{
    setFocusPolicy(Qt::NoFocus );
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    setText(text);
}

QSize Button::sizeHint() const
{
    QSize size = QPushButton::sizeHint();
    size.rheight() += button_size;
    size.rwidth() = qMax(size.width(), size.height());
    return size;
}

void Button::toggleFlag()
{
    emit flagToggled();
}

void Button::dblClick()
{
    emit dblClicked();
}
