#include"pathmaker.h"
#include<iostream>
#include<cstring>


int main( int argc, char **argv )
{
    size_t x_size, y_size, up_p = 40, sides_p = 30;
    assert( argc >= 3 );
    x_size = std::atoi( argv[1] );
    y_size = std::atoi( argv[2] );
    if( argc == 5 )
    {
        up_p = std::atoi( argv[3] );
        sides_p = std::atoi( argv[4] );
    }
    srand( time( NULL ) );
    PathMaker path( x_size, y_size, false, up_p, sides_p );
    for( int i = 0; i < x_size; i++ )
    {
        for( int j = 0; j < y_size; j++ )
            std::cout << (int)path[i][j];
        std::cout << std::endl;
    }
    std::cout << "Size: " << path.get_length() << std::endl;
    return 0;
}
