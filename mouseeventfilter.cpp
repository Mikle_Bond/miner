﻿#include <QtWidgets>
#include "mouseeventfilter.h"
#include "button.h"
MouseFilter::MouseFilter(QObject* pobj)
    : QObject(pobj)
{
}

bool MouseFilter::eventFilter(QObject* pobj, QEvent* pe)
{
    if (pe->type() == QEvent::MouseButtonPress) {
        if (static_cast<QMouseEvent*>(pe)->button() == Qt::RightButton) {
            static_cast<Button*>(pobj)->toggleFlag();
            return true;
        }
    }
    if (pe->type() == QEvent::MouseButtonDblClick) {
        if (static_cast<QMouseEvent*>(pe)->button() == Qt::LeftButton) {
            static_cast<Button*>(pobj)->dblClick();
            return true;
        }
    }
    return false;
}
