﻿#ifndef BUTTONHOLDER_H
#define BUTTONHOLDER_H

#include "dd.hpp"
#include "button.h"

class ButtonHolder;

template <>
struct DD_traits<ButtonHolder>
{
    using ret_type = Button;
};


class ButtonHolder : public DD<ButtonHolder>
{
public:
    ButtonHolder(size_t x, size_t y, int buttonsize);
    ~ButtonHolder();
};

#endif // BUTTONHOLDER_H
