#ifndef CONFIGREADER_H
#define CONFIGREADER_H

#include <qdebug.h>
#include <qlist.h>
#include <qfile.h>
#include <qtextstream.h>


struct Params
{
	size_t size_x;
	size_t size_y;
	size_t prob_x;
	size_t prob_y;
	double bombs;
};

class ConfigReader : private QList<Params>
{
    QString name_;
public:
    ConfigReader(const QString & name)
        : name_(name)
    {
		QFile f(name);
		if (!f.open(QIODevice::ReadOnly))
			qDebug() << "Couldn't open file" << name;
		QTextStream in(&f);
		while (!in.atEnd()) {
			QStringList cfg = in.readLine().split(' ');
			Params p;
			p.size_x = cfg[0].toInt();
			p.size_y = cfg[1].toInt();
			p.prob_x = cfg[2].toInt();
			p.prob_y = cfg[3].toInt();
			p.bombs = cfg[4].toDouble();
			this->push_front(p);
		}
	}

	Params top() const { return this->back(); }
	void pop() { this->pop_back(); }
	bool empty() const { return this->QList<Params>::empty(); }
    QString getName() const { return name_; }
};

#endif // CONFIGREADER_H
