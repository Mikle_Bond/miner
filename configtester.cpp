#include "configreader.h"

int main ()
{
	ConfigReader cfg("levels.cfg");
	while (!cfg.empty()) {
		Params p = cfg.top();
		qDebug() << p.size_x << p.size_y << p.prob_x << p.prob_y << p.bombs;
		cfg.pop();
	}
	return 0;
}
