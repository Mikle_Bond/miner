#Miner Game#

This is a simple miner game, but more realistic. 
You are the pioneer on the battlefield, and you need to gide your squad trough the minefield. In this game 
you need to find the path trouth mines, but you can not examine any field you wish, but only that lais in 
hand-lenght distant from the territory you discovered.

###Contribution###

Here is the UML of our project:

![UML-pic should be here][uml_svg]

But be careful, the picture can be outdated due to bitbucket.org caches images. To see most relevant version,
plese read the uml_diagram.pu file. Also you probably will need to learn PlantUML syntax first.
Visit the [offitial page](http://plantuml.com/) of the project.

[uml_svg]: http://plantuml.com/plantuml/svg/3SRB3O0W303GLNG1U_VetHb8-GGRMY2bZUlhExsTUcBFnPMtnsok4o2IHK-js36qmasDIxX6po3KYuBY2uBB_hfmo8HLKOxf7m00