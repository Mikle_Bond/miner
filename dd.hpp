#pragma once 
#include <cstdlib>
#include <algorithm>

/* dd.hpp
 * This is realization of the template class DD, whitch gives opportunity for the derived classes to use
 * implicitly created operator[][], that returns lvalue referance to the element of two-deimentional matrix.
 *
 * @author Mikle_Bond
 */

template <typename Derived>
struct DD_traits;

template <typename T>
class DD 
{
private:
	using T_ret_type = typename DD_traits<T>::ret_type;
protected:
	typename DD_traits<T>::ret_type **body_;
	size_t size_x, size_y;

	explicit DD(size_t x, size_t y) {
		// if we got exception here, so there were no changes
		body_ = new T_ret_type * [x];
		// if all is ok, it should be filled with nullptrs. just in case.
		std::fill_n(body_, x, nullptr);
		try {
			for (size_t i = 0; i < x; ++i) 
				body_[i] = new T_ret_type [y] ();
		} catch (...) {
			for (size_t i = 0; i < x; ++i)
				// it is safe to delete it, due to nulls.
				delete [] body_[i];
			throw;
		}
		size_x = x, size_y = y;
	}
	
	virtual ~DD() {
		for (int i = 0; i < size_x; ++i) 
			delete [] body_[i];
		delete [] body_;
	}
public:
	class RowLine {
		T_ret_type *row_;
	public:
		RowLine (T_ret_type *row) : row_(row) {}
		T_ret_type & operator[] (size_t j) {
			return row_[j];
		}
	};
//	friend class ::DD<T>::RowLine;

	RowLine operator[] (size_t i) {
		return RowLine(body_[i]);
	}
};
