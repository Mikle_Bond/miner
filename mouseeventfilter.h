﻿#ifndef MOUSEEVENTFILTER
#define MOUSEEVENTFILTER

class MouseFilter : public QObject
{
protected:
    virtual bool eventFilter(QObject*, QEvent*);
public:
    MouseFilter(QObject* pobj = 0);
};
#endif // MOUSEEVENTFILTER

