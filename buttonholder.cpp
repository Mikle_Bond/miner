﻿#include "buttonholder.h"

ButtonHolder::ButtonHolder(size_t x, size_t y, int buttonsize)
    : DD<ButtonHolder>(x, y)
{
    for(int i = 0; i < x; ++i) {
        for(int j = 0; j < y; ++j)
        {
            body_[i][j].pos_x = i;
            body_[i][j].pos_y = j;
            body_[i][j].setCheckable(true);
            body_[i][j].setMinimumSize(buttonsize, buttonsize);
            body_[i][j].setMaximumHeight(buttonsize);
            body_[i][j].setMaximumWidth(buttonsize);
        }
    }
}

ButtonHolder::~ButtonHolder()
{

}

