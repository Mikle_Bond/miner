#pragma once
/* field.h
 * This is the basic structure of the minefield. It provides user with information about one square at the 
 * minefield, such as is it closed, does it contain a mine etc.
 *
 * @author Mikle_Bond
 */

struct Field
{
	enum Value {
		MINE = -1
	};
	int opened = false;
	int value = 0;
	bool flag = false;
};


