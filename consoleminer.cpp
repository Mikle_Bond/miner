#include"gamematrix.h"
#include<iostream>
#include<cstring>

#define CHEAT 1


int main( int argc, char **argv )
{
    char command;
    size_t size_x, size_y, mine_n;
    size_t x, y;
    assert( argc == 3 );
    size_x = std::atoi( argv[1] );
    size_y = std::atoi( argv[2] );
    mine_n = size_x * size_y * 0.2;
    srand( time( NULL ) );
    GameMatrix game( size_x, size_y, mine_n );
    while( true )
    {
        std::cin >> command;
        if( command == 'q' )
            break;
        if( command == 'o' )
        {
            std::cin >> x >> y;
            if( !game.open( x, y ) )
            {
                std::cout << "GAME OVER" << std::endl;
                break;
            }
            if( game.isLevelSolved() )
                std::cout << "Task comleted, but you can continue searching" << std::endl;
        }
        if( command == 'f' )
        {
            std::cin >> x >> y;
            game.toggleFlag( x, y );
        }
        for( int i = 0; i < size_x; i++ )
        {
            for( int j = 0; j < size_y; j++ )
                if( game[i][j].flag )
                    std::cout << 'f';
                else
                    if( game[i][j].opened )
                        std::cout << game[i][j].value;
                    else
                        std::cout << "#";
            std::cout << std::endl;
        }
#if CHEAT
        std::cout << "CHEAT:" << std::endl;
        for( int i = 0; i < size_x; i++ )
        {
            for( int j = 0; j < size_y; j++ )
                if( game[i][j].value == Field::MINE )
                    std::cout << 'm';
                else
                    std::cout << game[i][j].value;
            std::cout << std::endl;
        }
#endif

    }
    return 0;
}


