#include"pathmaker.h"

void PathMaker::fillPath()
{
    enum { X, Y };
    int prev;  //what movement we did before
    int next;  //what will we do next
    int current[2];  //current position
    bool youCanMove[4] = {};   //where you can move in this position
    //init
    length_ = 0;
    for( int i = 0; i < size_x; i++ )
        for( int j = 0; j < size_y; j++ )
            body_[i][j] = false;
    current[Y] = 0;
    current[X] = rand() % ( size_y - 1 );  //cause we leave thr rightest column
    if( double_path_ )
        body_[0][current[X] + 1] = true;  //or we'll never paint it
    prev = UP;      //what if we'll go right?
    //main part
    while( current[Y] != size_x - 1 )
    {
        body_[current[Y]][current[X]] = true;            //first of all you must paint current position
        length_++;
        if( double_path_ )
            body_[current[Y] + 1][current[X] + 1] = true;    //and the one from right above
        //filling youCanMove and chosing next
        youCanMove[UP] = true;
        youCanMove[RIGHT] = ( current[X] < size_y - 2 ) ? true : false;  //1 extra cell left cause we paint two
        youCanMove[DOWN] = false;
        youCanMove[LEFT] = ( current[X] > 0 ) ? true : false;
        youCanMove[reverseDir( prev )] = false;                        //cause it useless to move back
        next = chooseNext( youCanMove );
        //extra paints if we double ( depends on var next )
        if( double_path_ )
        {
            if( next == RIGHT && prev == UP )
                body_[current[Y] + 1][current[X]] = true;   //cause if we paint right and above, we will never paint this one
            if( next == UP && prev == RIGHT )
                body_[current[Y]][current[X] + 1] = true;   //the same reason
        }
        //prepare varibles for the next iteration
        prev = next;
        switch( next )
        {
            case UP:
                current[Y]++;
                break;
            case RIGHT:
                current[X]++;
                break;
            case DOWN:
                assert(0);
            case LEFT:
                current[X]--;
                break;
        }
    }
    body_[current[Y]][current[X]] = true;  //paint the last one
    length_++;
}

int PathMaker::chooseNext( bool *youCanMove )
{
    int r = 0;  //I shouldn't call it so
    if( youCanMove[RIGHT] && youCanMove[LEFT] )
    {
        r = rand() % ( up_p_ + sides_p_ * 2 );
        if( r < up_p_ )
            return UP;
        else
            if( r < sides_p_ + up_p_ )
                return LEFT;
            else
                return RIGHT;
    }
    else
        if( youCanMove[RIGHT] || youCanMove[LEFT] )
        {
            r = rand() % ( up_p_ + sides_p_ );
            if( r < up_p_ )
                return UP;
            else
                if( r < up_p_ + sides_p_ )
                    return youCanMove[RIGHT] * RIGHT + youCanMove[LEFT] * LEFT;
                else
                    assert( 0 );
        }
        else
            return UP;
    assert( 0 );
}
