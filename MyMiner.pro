#-------------------------------------------------
#
# Project created by QtCreator 2016-05-21T19:15:56
#
#-------------------------------------------------

QT       += core
QT	 += widgets
QT       -= gui

TARGET = MyMiner
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    minepaint.cpp \
    button.cpp \
    gamematrix.cpp \
    pathmaker.cpp \
    buttonholder.cpp \
    score.cpp \
    mouseeventfilter.cpp

HEADERS += \
    minepaint.h \
    button.h \
    dd.hpp \
    field.h \
    gamematrix.h \
    pathmaker.h \
    buttonholder.h \
    configreader.h \
    score.h \
    mouseeventfilter.h
